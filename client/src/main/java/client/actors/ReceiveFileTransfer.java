package client.actors;

import client.utlity.Checksum;
import client.exceptions.CryptoException;
import client.utlity.EncryptDecrypt;
import client.utlity.GeneralUtility;

import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;


/**
 * Sources used to code this:
 * https://howtodoinjava.com/java/multi-threading/callable-future-example/
 * https://bitek.dev/blog/java_threading_shared_data_tutorial/
 * https://gist.github.com/CarlEkerot/2693246
 */

/**
 * Receives & stores file from server and decrypts it if necessary.
 */

public class ReceiveFileTransfer implements Runnable {
    private final String[] splitStr;
    private final GeneralUtility generalUtility;
    private final SecretKey secretKey;
    private final Socket socketFileTransfer;
    private String receivedCheckSum = "";
    //File path
    private String path = "./files/client_files_received/";

    public ReceiveFileTransfer(String[] splitStr, SecretKey secretKey, Socket socketFileTransfer) {
        this.splitStr = splitStr;
        this.secretKey = secretKey;
        this.socketFileTransfer = socketFileTransfer;
        this.generalUtility = new GeneralUtility();
    }


    @Override
    public void run() {
        String fileName = "";
        boolean encrypted = false;
        if (splitStr.length >= 4) {
            fileName = splitStr[1];
            path += fileName;
            int fileLength = generalUtility.tryParse(splitStr[3]);
            if (generalUtility.tryParse(splitStr[4]) == 1) encrypted = true;
            System.out.println("FileName: " + fileName);
            System.out.println("FileSize: " + fileLength + " bytes");
            System.out.println("FileEncrypted: " + encrypted);
            saveFileFromServer(fileName, fileLength);

            if (encrypted) {
                decryptedFile();
            }
            if (!receivedCheckSum.equals("")) {
                checkCheckSum();
            }
        }
    }

    private void saveFileFromServer(String fileName, int fileLength) {
        //Creating file to print location later.
        File file = new File(path);
        //Create parent directories if not exists
        file.getParentFile().mkdirs();
        try {
            DataInputStream dataInputStream = new DataInputStream(socketFileTransfer.getInputStream());
            receivedCheckSum = dataInputStream.readUTF();
            FileOutputStream fileOutputStream = new FileOutputStream(path);

            byte[] buffer = new byte[fileLength];

            int read = 0;
            int totalRead = 0;
            int remaining = fileLength;

            while ((read = dataInputStream.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
                totalRead += read;
                remaining -= read;
                System.out.println("Total read: " + totalRead + " bytes" + " remaining bytes: " + remaining);
                //write bytes to file.
                fileOutputStream.write(buffer, 0, read);
                //print write to console
                System.out.println("Content of file: ");
                System.out.write(buffer, /* start */ 0, /* length */ read);
            }
            fileOutputStream.close();
            System.out.println("\nFile: " + fileName + " | Stored at: " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void decryptedFile() {
        File encryptedFile = new File(path);
        //Change path for decrypted file
        path = path.replaceAll(".encrypted", "");
        File decryptedFile = new File(path);
        try {
            EncryptDecrypt.decryptFile(secretKey, encryptedFile, decryptedFile);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        System.out.println("Decrypted file stored at: " + decryptedFile.getAbsolutePath());
    }

    private void checkCheckSum() {
        String result = "Checksum match: ";
        String checkSum = Checksum.generateCheckSum(path);
        if (receivedCheckSum.equals(checkSum)) {
            result += "true.";
        } else {
            result += "false.";
        }
        System.out.println(result);
    }
}
