package client.actors;

import client.utlity.EncryptDecrypt;
import client.utlity.GeneralUtility;
import client.utlity.SharedKeysStorage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Sends messages for the client. Kills the thread when the socket disconnects.
 */

public class SendMessageActor implements Runnable {
    private final PrintWriter printWriter;
    private final Socket socket;
    private final KeyPair keyPair;
    private final FileTransferManager fileTransferManager;
    private final ExecutorService threadPool;
    private Scanner scanner;

    public SendMessageActor(PrintWriter writer, Socket socket, KeyPair keyPair, Socket socketFileTransfer) {
        this.socket = socket;
        this.printWriter = writer;
        this.keyPair = keyPair;
        this.fileTransferManager = new FileTransferManager(socketFileTransfer, this);
        threadPool = Executors.newFixedThreadPool(4);
    }

    @Override
    public void run() {
        String userInput = "";

        while (socket.isConnected()) {
            userInput = readLine();
            String[] splitStr = userInput.split("\\s+");

            switch (splitStr[0]) {
                case "QUIT" -> quit(userInput);
                case "SFT" -> sendFileToUserEncrypted(splitStr);
                case "PM" -> sendPMEncrypted(splitStr, userInput);
                case "help" -> help();
                default -> sendCommand(userInput);
            }
        }
    }

    private void quit(String command) {
        sendCommand(command);
        scanner.close(); //Closes thread blocking scanner.
    }

    private void help() {
        System.out.println("Command: [RFFA] [senderName] = to accept FileTransfer");
        System.out.println("Command: [SFT] [user] = sends file to user");
        System.out.println("Command: [SGK] [groupName] = broadcasts status of vote to kick procces of group");
        System.out.println("Command: [SGKM] [groupName] [userNameOfKick] = starts a vote to kick a user in a group");
        System.out.println("Command: [KGRPM] [groupName] [y/n] = command to vote in a vote process");
        System.out.println("Command: [LGRPS] = returns a list of all groups");
        System.out.println("Command: [ULST] = returns a list of active users");
        System.out.println("Command: [LGRP] [groupName] = leave group");
        System.out.println("Command: [JGRP] [groupName] = join group");
        System.out.println("Command: [MGRP] [groupName] = message group");
        System.out.println("Command: [DGRP] [groupName] = delete group");
        System.out.println("Command: [CGRP] [groupName] = create group");
        System.out.println("Command: [QUIT] = logout");
        System.out.println("Command: [TERM] = terminate connection");
        System.out.println("Command: [PM] [who] = private message");
    }


    private void sendFileToUserEncrypted(String[] splitStr) {
        if (splitStr.length >= 2) {
            String targetUsername = splitStr[1];
            if (SharedKeysStorage.checkIfSharedKeyExists(targetUsername)) {
                fileTransferManager.addSendFileTransferProces(true, targetUsername, SharedKeysStorage.getUserSessionKey(targetUsername));
            } else {
                createSession(targetUsername, false);
                //Setting task in the que
                threadPool.submit(() -> {
                    waitForSharedKey(10, targetUsername);
                    if (SharedKeysStorage.checkIfSharedKeyExists(targetUsername)) {
                        fileTransferManager.addSendFileTransferProces(true, targetUsername, SharedKeysStorage.getUserSessionKey(targetUsername));
                    } else { //Failed to get shared key, sending file unencrypted.
                        System.err.println("Failed to get shared key from: " + targetUsername);
                    }
                });
            }
        }
    }

    private void waitForSharedKey(int timeInSeconds, String targetUsername) {
        int counter = 0;
        while (counter != timeInSeconds) {
            try {
                TimeUnit.SECONDS.sleep(1);
                if (SharedKeysStorage.checkIfSharedKeyExists(targetUsername)) {
                    break;
                }
                counter++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendPMEncrypted(String[] splitStr, String userInput) {
        if (splitStr.length >= 1) {
            String targetUserName = splitStr[1];
            if (SharedKeysStorage.checkIfSharedKeyExists(targetUserName)) {
                sendEncryptedMessageWithSharedKey(targetUserName, userInput);
            } else {
                createSession(splitStr[1], false);
                //Setting task in the que
                threadPool.submit(() -> {
                    waitForSharedKey(10, targetUserName);
                    if (SharedKeysStorage.checkIfSharedKeyExists(targetUserName)) {
                        sendEncryptedMessageWithSharedKey(targetUserName, userInput);
                    } else {
                        System.err.println("Could not PM, no shared key!");
                    }
                });
            }
        } else {
            System.err.println("Please enter user name");
        }
    }

    private void sendEncryptedMessageWithSharedKey(String targetUserName, String line) {
        String command = "PM " + targetUserName;
        String message = GeneralUtility.removeWord(line, command);
        try {
            message = EncryptDecrypt.encryptString(message, SharedKeysStorage.getUserSessionKey(targetUserName));
            sendCommand(command + " " + message);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void sendCommand(String command) {
        printWriter.println(command);
        printWriter.flush();
    }

    private String readLine() {
        scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public void createSession(String targetUserName, boolean receivedPublicKey) {
        System.out.println("Creating session for " + targetUserName);
        sendCommand("CSW " + targetUserName + " " + receivedPublicKey + " " + GeneralUtility.toHexString(keyPair.getPublic().getEncoded())); //Create season with
    }
}
