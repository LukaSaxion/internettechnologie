package client.actors;

import javax.crypto.SecretKey;
import java.net.Socket;
import java.util.UUID;

/**
 * Manages the threads for the ReceiveFileTranfer.class & SendFileTransfer.class.
 */

public class FileTransferManager {
    private final Socket socketFileTransfer;
    private final SendMessageActor sendMessageActor;
   private Thread sendfileTransferThread = null;
    private Thread receiveFileTransferThread = null;
    public FileTransferManager(Socket socketFileTransfer, SendMessageActor sendMessageActor) {
        this.socketFileTransfer = socketFileTransfer;
        this.sendMessageActor = sendMessageActor;
    }

    public void addReceiveFileTransferProces( String[] splitStr, SecretKey secretKey) {
        if (receiveFileTransferThread == null) {
            startReceiveFileTransferThread( splitStr, secretKey, socketFileTransfer);
            System.out.println(receiveFileTransferThread);
        } else if (!receiveFileTransferThread.isAlive()) {
            startReceiveFileTransferThread(splitStr, secretKey, socketFileTransfer);
        } else {
            sendMessageActor.sendCommand("There already is a receiveFileTransfer procces active!");
        }
    }

    public void startReceiveFileTransferThread(String[] splitStr, SecretKey secretKey, Socket socketFileTransfer) {
        ReceiveFileTransfer receiveFileTransfer = new ReceiveFileTransfer(splitStr, secretKey, socketFileTransfer);
        receiveFileTransferThread = new Thread(receiveFileTransfer, "Client receiveFileTransfer procces " + " " + UUID.randomUUID());
        receiveFileTransferThread.start();
    }

    public void addSendFileTransferProces( boolean encrypted, String targetUserName, SecretKey secretKey) {
        if (sendfileTransferThread == null) {
            startSendFileTransferThread( encrypted, targetUserName, secretKey);
            System.out.println(sendfileTransferThread);
        } else if (!sendfileTransferThread.isAlive()) {
            startSendFileTransferThread( encrypted, targetUserName, secretKey);
        } else {
            sendMessageActor.sendCommand("There already is a sendFileTransfer procces active!");
        }
    }

    public void startSendFileTransferThread(boolean encrypted, String targetUserName, SecretKey secretKey) {
        SendFileTransfer sendFileTransfer = new SendFileTransfer(secretKey,socketFileTransfer, sendMessageActor, encrypted, targetUserName);
        sendfileTransferThread = new Thread(sendFileTransfer, "Client sendFileTransfer procces " + " " + UUID.randomUUID());
        sendfileTransferThread.start();
    }
}
