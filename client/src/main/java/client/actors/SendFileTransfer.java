package client.actors;

import client.utlity.Checksum;
import client.exceptions.CryptoException;
import client.utlity.EncryptDecrypt;

import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * Sources used to code this:
 * https://howtodoinjava.com/java/multi-threading/callable-future-example/
 * https://bitek.dev/blog/java_threading_shared_data_tutorial/
 * https://gist.github.com/CarlEkerot/2693246
 */

/**
 * Sends file to server and encrypts it if necessary.
 */

public class SendFileTransfer implements Runnable {
    private final SecretKey secretKey;
    private final Socket socketFileTransfer;
    private final SendMessageActor sendMessageActor;
    private final boolean encrypted;
    private final String targetUserName;

    public SendFileTransfer(SecretKey secretKey, Socket socketFileTransfer, SendMessageActor sendMessageActor, boolean encrypted, String targetUserName) {
        this.secretKey = secretKey;
        this.socketFileTransfer = socketFileTransfer;
        this.sendMessageActor = sendMessageActor;
        this.encrypted = encrypted;
        this.targetUserName = targetUserName;
    }


    @Override
    public void run() {
        String filename = "testFile2.txt";
        String path = "./client_files/" + filename;
        File myFile = new File(path); //Original file
        String checkSum = Checksum.generateCheckSum(path);
        System.out.println("Checksum: " + checkSum);
        if (encrypted) {
            //Give new fileName and path to encrypted file
            path += ".encrypted";
            File encryptedFile = new File(path);
            myFile = encryptFile(myFile, encryptedFile);
        }
        sendMessageActor.sendCommand("SFT " + myFile.getName() + " " + targetUserName + " " + myFile.length() + " " + (encrypted ? 1 : 0));

        System.out.println("File length: " + myFile.length());

        System.out.println(myFile.getAbsolutePath());
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(socketFileTransfer.getOutputStream());
            //Write checksum to DOS
            dataOutputStream.writeUTF(checkSum);
            FileInputStream fileInputStream = new FileInputStream(path);

            byte[] buffer = new byte[(int) myFile.length()];

            System.out.println("File content:");
            //Print data to console
            System.out.println(new String(Files.readAllBytes(Paths.get(path))));

            //Write data to stream
            while (fileInputStream.read(buffer) > 0) {
                dataOutputStream.write(buffer);
            }

            System.out.println("File: " + myFile.getName() + " is send");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File encryptFile(File inputFile, File encryptedFile) {
        try {
            EncryptDecrypt.encryptFile(secretKey, inputFile, encryptedFile);
        } catch (CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return encryptedFile;
    }
}
