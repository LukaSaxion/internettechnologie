package client.actors;

import client.Client;
import client.utlity.EncryptDecrypt;
import client.utlity.GeneralUtility;
import client.utlity.SharedKeysStorage;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

/**
 * Receives message for the client and prints it into the console. Kills the thread when a line equals "200 Goodbye".
 */

public class ReceiveMessageActor implements Runnable {
    private final Socket socket;
    private final FileTransferManager fileTransferManager;
    private final SendMessageActor sendMessageActor;
    private final GeneralUtility generalUtility;
    private final KeyPair keyPair;
    private final Client client;
    private BufferedReader reader;

    public ReceiveMessageActor(SendMessageActor sendMessageActor, Socket socket, KeyPair keyPair, Client client, Socket socketFileTransfer) {
        this.socket = socket;
        this.sendMessageActor = sendMessageActor;
        this.generalUtility = new GeneralUtility();
        this.keyPair = keyPair;
        this.client = client;
        //Get input- en output stream from the socket
        try {
            InputStream inputStream = socket.getInputStream();
            //Get string from inputStream
            this.reader = new BufferedReader(new InputStreamReader(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileTransferManager = new FileTransferManager(socketFileTransfer, sendMessageActor);
    }

    @Override
    public void run() {
        String line = "";
        try {
            while (socket.isConnected()) {
                try {
                    line = reader.readLine();
                } catch (SocketException e) {
                    e.printStackTrace();
                    break;
                }
                //Split split in string[] by " "
                String[] splitStr = line.split("\\s+");

                if ("200 Goodbye".equals(line)) {
                    logOutUser();
                }
                switch (splitStr[0]) {
                    case "[PM]" -> receivesPM(splitStr);
                    case "RECEIVE_FILE" -> fileTransferManager.addReceiveFileTransferProces(splitStr, SharedKeysStorage.getUserSessionKey(splitStr[2]));
                    case "PUBLICKEY" -> receivePublicKeyAndCreateSharedKey(splitStr);
                    case "PING" -> sendPong();
                    case "REMOVE_SHARED_KEY" -> removeSharedKey(splitStr);
                    default -> System.out.println(line);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logOutUser() {
        System.out.println("successfully logged out!");
        client.setTerminated(true);
    }

    private void removeSharedKey(String[] splitStr) {
        if (splitStr.length >= 2){
            SharedKeysStorage.removeUserSessionKey(splitStr[1]);
        }
    }

    /**
     * Creates a shared AES key between users.
     *
     * @param receivedPublicKey received public keu from user.
     * @return shared session Key
     */
    public SecretKey createSharedKey(PublicKey receivedPublicKey) {
        try {
            //Create keyAgreement
            KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");
            //Set own private key
            keyAgreement.init(keyPair.getPrivate());
            //Add received publickey
            keyAgreement.doPhase(receivedPublicKey, true);
            //Generate shared secret byte[]
            byte[] secret = keyAgreement.generateSecret();
            //Create shared secret
            return new SecretKeySpec(Arrays.copyOfRange(secret, 0, 32), "AES");
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    //todo clean up souts
    private void receivePublicKeyAndCreateSharedKey(String[] splitStr) {
        if (splitStr.length >= 3) {
            try {
                String targetUserName = splitStr[1];
                System.out.println("getting Public key from " + targetUserName);
                //receiving public key string & convert to byte[] from sender.
                byte[] publicKeyEnc = generalUtility.toBytes(splitStr[3]);
                KeyFactory receivedPublicKeyFac = KeyFactory.getInstance("DH");
                X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(publicKeyEnc);
                PublicKey receivedPublicKey = receivedPublicKeyFac.generatePublic(x509KeySpec);
                System.out.println("Public key from user: " + targetUserName);
                //create shared session key
                SecretKey sharedKey = createSharedKey(receivedPublicKey);
                SharedKeysStorage.addToUserSharedKey(targetUserName, sharedKey);
                System.out.println("Shared key for user: " + targetUserName + " key: " + sharedKey.getEncoded().length);
                //Send public key to sender if the sender doesnt have a public key
                if (splitStr[2].equals("false")) sendMessageActor.createSession(targetUserName, true);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
    }

    private void receivesPM(String[] splitStr) {
        if (splitStr.length >= 2) {
            try {
                String line = EncryptDecrypt.decryptString(splitStr[2], SharedKeysStorage.getUserSessionKey(splitStr[1]));
                System.out.println(splitStr[0] + " " + splitStr[1] + ": " + line);
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendPong() {
        sendMessageActor.sendCommand("PONG");
    }
}
