package client;

import client.actors.ReceiveMessageActor;
import client.actors.SendMessageActor;
import client.utlity.GenAsymmetric;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Receives and send messages to the connected server socket.
 */
public class Client {
    //Reads input
    private InputStream inputStream;
    private BufferedReader reader;
    //Sends output
    private OutputStream outputStream;
    private PrintWriter writer;

    //    private final String secretKey = UUID.randomUUID().toString().substring(0, 32);
    private boolean loggedOut = true;
    private volatile boolean terminated = false; //volatile ensures that thread which runs your while loop does not have stale value of the terminated variable
    private final String serverHost = "127.0.0.1";
    private int errorCount = 0;




    // Sources:
    // https://www.chegg.com/homework-help/questions-and-answers/using-java-assignment-implement-basic-key-transport-protocol-key-aes-sent-client-server-us-q47638952
    // http://www.java2s.com/example/java/network/creates-a-publicprivate-key-pair-using-rsa.html
    // https://stackoverflow.com/questions/2411096/how-to-recover-a-rsa-public-key-from-a-byte-array
    public static void main(String[] args) {
        new Client().run();
    }

    public void run() {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        Socket socket = null;
        Socket socketFileTransfer = null;
        KeyPair keyPair = null;

        try { //Creating RSA keys
            keyPair = GenAsymmetric.generateRSAKkeyPair();
        } catch (Exception e) {
            e.printStackTrace();
        }

        while (!terminated) {
            if (errorCount >= 5) {
                terminated = true;
            }
            if (loggedOut) {
                try {
                    //Connecting to server
                    socket = new Socket(serverHost, 3001);
                    socketFileTransfer = new Socket(serverHost, 3002);
                    //Get input- en output stream from the socket
                    InputStream inputStream = socket.getInputStream();
                    OutputStream outputStream = socket.getOutputStream();

                    //Send strings to outputStreams
                    writer = new PrintWriter(outputStream);
                    //Get strings from inputStream
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    System.out.println("Use 'CONN username' to login");
                    login();
                    //Init actor classes.
                    var sendMessageActor = new SendMessageActor(writer, socket, keyPair, socketFileTransfer);
                    var receiveMessageActor = new ReceiveMessageActor(sendMessageActor, socket, keyPair, this, socketFileTransfer);
                    //Starting send & receive threads
                    threadPool.submit(sendMessageActor);
                    threadPool.submit(receiveMessageActor);
                } catch (UnknownHostException e) {
                    System.err.println("Don't know about host: " + serverHost);
                    errorCount++;
                } catch (IOException e) {
                    writer.close();
                    System.err.println("Couldn't get I/O for "
                            + "the connection to: " + serverHost);
                    errorCount++;
                }
            }
        }
        threadPool.shutdown();
        System.out.println("Program terminated");
    }

    private String readLine() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private void login() {
        String line = "";
        while (true) {
            try {
                if (reader.ready()){
                    line = reader.readLine();
                    System.out.println(line);
                    if (line.contains("200")) {
                        loggedOut = false;
                        System.out.println("You have successfully logged in!");
                        break;
                    } else {
                        sendCommand();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void sendCommand() {
        String userInput = readLine();
        writer.println(userInput);
        writer.flush();
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }
}
