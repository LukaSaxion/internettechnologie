package client.utlity;

import client.exceptions.CryptoException;

import javax.crypto.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Encrypts & decrypts strings & files.
 * A part of the code comes from source: https://www.codejava.net/coding/file-encryption-and-decryption-simple-example
 */

public class EncryptDecrypt {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";

    // source: https://www.codejava.net/coding/file-encryption-and-decryption-simple-example
    public static void encryptFile(SecretKey secretKey, File inputFile, File outputFile)
            throws CryptoException {
        doFileCrypto(Cipher.ENCRYPT_MODE, secretKey, inputFile, outputFile);
    }

    // source: https://www.codejava.net/coding/file-encryption-and-decryption-simple-example
    public static void decryptFile(SecretKey secretKey, File inputFile, File outputFile)
            throws CryptoException {
        doFileCrypto(Cipher.DECRYPT_MODE, secretKey, inputFile, outputFile);
    }

    // source: https://www.codejava.net/coding/file-encryption-and-decryption-simple-example
    private static void doFileCrypto(int cipherMode, SecretKey secretKey, File inputFile,
                                     File outputFile) throws CryptoException {
        try {
//            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }

    //Source used: https://www.baeldung.com/java-aes-encryption-decryption
    public static String encryptString(String input, SecretKey secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(ALGORITHM);

        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] cipherText = cipher.doFinal(input.getBytes());
        return Base64.getEncoder()
                .encodeToString(cipherText);
    }

    //Source used: https://www.baeldung.com/java-aes-encryption-decryption
    public static String decryptString(String cipherText, SecretKey secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] plainText = cipher.doFinal(Base64.getDecoder()
                .decode(cipherText));
        return new String(plainText);
    }
}
