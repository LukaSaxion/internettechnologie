package client.utlity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Checksum {
    /**
     * Generates an MD5 checksum as a String from file path.
     * Source: https://stackoverflow.com/questions/304268/getting-a-files-md5-checksum-in-java
     * @param path path to the file.
     * @return ChecksumString on succes.
     */
    public static String generateCheckSum(String path) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(Paths.get(path)));
            byte[] digest = md.digest();
            result = Arrays.toString(digest);
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
