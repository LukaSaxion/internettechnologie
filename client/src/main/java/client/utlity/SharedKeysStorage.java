package client.utlity;

import javax.crypto.SecretKey;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Functions as sharedKeys storage.
 */
public class SharedKeysStorage {
    private static Map<String, SecretKey> userSharedKeys = Collections.synchronizedMap(new HashMap<>());

    public static void addToUserSharedKey(String userName, SecretKey sessionKey) {
        System.out.println("Added key for: " + userName);
        userSharedKeys.put(userName, sessionKey);
    }

    public static SecretKey getUserSessionKey(String userName) {
        return userSharedKeys.get(userName);
    }
    //todo nog implementeren nadat user & andere disconnects
    public static void removeUserSessionKey(String userName) {
         userSharedKeys.remove(userName);
    }

    public static boolean checkIfSharedKeyExists(String userName) {
        return userSharedKeys.containsKey(userName);
    }
}
