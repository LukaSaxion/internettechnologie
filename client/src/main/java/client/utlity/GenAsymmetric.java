package client.utlity;

import java.security.KeyPair;
import java.security
        .KeyPairGenerator;
import java.security
        .SecureRandom;
//import javax.xml.bind
//        .DatatypeConverter;

//Source: https://www.geeksforgeeks.org/asymmetric-encryption-cryptography-in-java/

// Class to create an asymmetric key
public class GenAsymmetric {

    private static final String RSA
            = "DH";

    // Generating public and private keys
    // using RSA algorithm.
    public static KeyPair generateRSAKkeyPair()
            throws Exception
    {
        SecureRandom secureRandom
                = new SecureRandom();

        KeyPairGenerator keyPairGenerator
                = KeyPairGenerator.getInstance(RSA);

        keyPairGenerator.initialize(
                2048, secureRandom);

        return keyPairGenerator
                .generateKeyPair();
    }
}