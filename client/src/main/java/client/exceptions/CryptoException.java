package client.exceptions;

/**
 * General cryptoException.
 * source: https://www.codejava.net/coding/file-encryption-and-decryption-simple-example
 */

public class CryptoException extends Exception {

    public CryptoException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
