package server.model;

import server.actors.KickUser;
import server.utlity.ServerUtil;

import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Group {
    private String groupName;
    private Map<String, Socket> clients;
    private HashMap<String, KickUser> kickUserHashMap;
    private Thread thread1;
    private KickUser kickUser;

    public Group(Socket owner, String ownerName, String groupName) {
        this.groupName = groupName;
        this.clients = Collections.synchronizedMap(new HashMap<>());
        clients.put(ownerName, owner);
        kickUserHashMap = new HashMap<>();
    }

    public void addKickUserProcces(String userName, Long durationLength, ServerUtil serverUtil) {
        if (thread1 == null) {
            startKickThread(userName, durationLength, serverUtil);
            System.out.println(thread1);
        } else if (!thread1.isAlive()) {
            startKickThread(userName, durationLength, serverUtil);
        } else {
            serverUtil.broadcastMessageToGroup(groupName, "There already is a kick procces active!");
        }
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        kickUser.getStatus();
    }

    private void startKickThread(String userName, Long durationLength, ServerUtil serverUtil) {
        kickUser = new KickUser(clients.size(), groupName, userName, durationLength, serverUtil);
        thread1 = new Thread(kickUser, "Kick procces " + userName + " " + UUID.randomUUID());
        thread1.start();
    }

    public void voteOnKick(String voterName, boolean vote, ServerUtil serverUtil) {
        if (thread1 == null) {
            serverUtil.sendMessageToClient("There is no vote procces active at the moment!", voterName);
        } else if (thread1.isAlive()) {
            kickUser.addVote(voterName, vote);
            serverUtil.sendMessageToClient("You voted: " + vote, voterName);
        } else {
            serverUtil.sendMessageToClient("There is no vote procces active at the moment!", voterName);
        }
    }

    public void statusVoteToKick(ServerUtil serverUtil) {
        if (thread1 == null) {
            serverUtil.broadcastMessageToGroup(groupName, "There is no vote procces active at the moment!");
        } else if (thread1.isAlive()) {
            kickUser.getStatus();
        } else {
            serverUtil.broadcastMessageToGroup(groupName, "There is no vote procces active at the moment!");
        }
    }

    public Map<String, Socket> getClients() {
        return clients;
    }

    public void setClients(Map<String, Socket> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupName='" + groupName + '\'' +
                '}';
    }
}
