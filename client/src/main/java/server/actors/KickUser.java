package server.actors;

import server.utlity.ServerUtil;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Sources used to code this:
 * https://howtodoinjava.com/java/multi-threading/callable-future-example/
 * https://bitek.dev/blog/java_threading_shared_data_tutorial/
 */

/**
 * Handles group vote kicks.
 */

public class KickUser implements Runnable {
    private final int members;
    private final String groupName;
    private final String userName;
    private HashMap<String, Boolean> votes;
    private Long durationLength;
    private final ServerUtil serverUtil;
    private long endTime = 0;

    public KickUser(int members, String groupName, String userName, Long durationLength, ServerUtil serverUtil) {
        this.members = members;
        this.groupName = groupName;
        this.userName = userName;
        this.durationLength = durationLength;
        this.serverUtil = serverUtil;
        votes = new HashMap<>();
    }


    @Override
    public void run() {
        durationLength = TimeUnit.MINUTES.toMillis(durationLength);
        long currentTime = System.currentTimeMillis();
        //determine end time
        endTime = currentTime + durationLength;
        //Determine time for heads up message
        long timeLeftToVoteAfterHeadsUpMessage = durationLength / 10;
        long timeForHeadsUpMessage = endTime - (timeLeftToVoteAfterHeadsUpMessage);
        String message = "";

        //Keeping thread alive & sending headsup message at 1/10 of the duration.
        while (System.currentTimeMillis() <= endTime) {
            if (System.currentTimeMillis() == timeForHeadsUpMessage) {
                message = "Vote to kick only lasts " + TimeUnit.MILLISECONDS.toSeconds(timeLeftToVoteAfterHeadsUpMessage) + " more seconds";
                serverUtil.broadcastMessageToGroup(groupName, message);
            }
        }

        //count votes
        int votesTrue = countTrueVotes();
        //determine kick
        if (votesTrue > (members * 0.50)) {
            serverUtil.removeUserFromGroup(groupName, userName, "User " + userName +
                    " is kicked from the group! With " + votesTrue + "/" + members + " votes.");
            serverUtil.sendMessageToClient("You have been kicked from the group " +
                    groupName + "! With " + votesTrue + "/" + members + " votes.", userName);
        } else {
            serverUtil.broadcastMessageToGroup(groupName, "Vote to kick: " + userName + " failed!");
        }
    }

    public synchronized void addVote(String voterName, boolean vote) {
        this.votes.put(voterName, vote);
    }

    public synchronized void getStatus() {
        String message = "Current votes for kick " + userName + ": " + countTrueVotes() + "/" + members +
                " votes. Time left to vote: " + TimeUnit.MILLISECONDS.toSeconds((endTime - System.currentTimeMillis())) + " seconds.";
        serverUtil.broadcastMessageToGroup(groupName, message);
    }

    private int countTrueVotes() {
        AtomicInteger result = new AtomicInteger();
        //count votes
        votes.forEach(((s, aBoolean) -> {
            if (aBoolean) {
                result.getAndIncrement();
            }
        }));
        return result.get();
    }
}
