package server.actors;

import server.utlity.GeneralUtility;
import server.utlity.ServerUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.UUID;

/**
 * Handles client (user) requests.
 */

public class ServerMessageActor implements Runnable {
    private final BufferedReader bufferedReader;
    private final Socket client;
    private final Socket fileTransferSocket;
    //Keeps track of last pong reponse of client.
    private long lastPong;
    private final int pongWithinSec;
    private final ServerUtil serverUtil;
    private final boolean shouldPing;
    private final String userName;
    private final String CMD_PONG = "PONG";
    private Thread fileTransferManagerThread;

    public ServerMessageActor(BufferedReader bufferedReader, Socket client, String userName, int pongWithinSec, ServerUtil serverUtil, boolean shouldPing, Socket fileTransferSocket) {
        this.bufferedReader = bufferedReader;
        this.client = client;
        this.userName = userName;
        this.pongWithinSec = (pongWithinSec * 1000);
        this.serverUtil = serverUtil;
        this.shouldPing = shouldPing;
        this.fileTransferSocket = fileTransferSocket;

    }

    @Override
    public void run() {
        String line = "";
        lastPong = System.currentTimeMillis();
        while (!client.isClosed()) {
            try {
                //reseting line to prevent repeating last task
                line = "";
                //to stop the loop from staying stuck on readline
                if (bufferedReader.ready()) {
                    line = bufferedReader.readLine();
                    if (!line.equals(CMD_PONG)) {
                        System.out.println(userName + " " + line);
                    }
                }
                //slicing line with " "
                String[] splitStr = line.split("\\s+");
                if (shouldPing) {
                    pong(splitStr[0]);
                }
                if (!line.equals("") && !line.equals(CMD_PONG)) {
                    commandSwitch(splitStr, line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("SERVER MESSAGE ACTOR closed for " + userName);
    }

    private void commandSwitch(String[] splitStr, String line) {
        switch (splitStr[0]) {
            case "SGK" -> statusGroupKick(splitStr);                            //Returns status of group vote to kick to user.
            case "SGKM" -> startVoteToKickInGroup(splitStr);                    //StartVoteToKickInGroup.
            case "KGRPM" -> kickGroupMember(splitStr);                          //Vote to member from group.
            case "CGRP" -> createGroup(splitStr);                               //Create group.
            case "LGRP" -> removeUserFromGroup(splitStr);                       //Leave group.
            case "JGRP" -> addUserToGroup(splitStr);                            //Join group.
            case "MGRP" -> messageGroup(splitStr, line);                        //Message group.
            case "ULST" -> returnAllUserList();                                 //Returns list of a users.
            case "BCST" -> broadcastToAllUsers(splitStr, line);                 //Broadcast message to all users.
            case "PM" -> pmUser(splitStr, line);                                //Private message user.
            case "QUIT" -> quitUser();                                          //Quite user.
            case "LGRPS" -> serverUtil.listAllGroupsToUser(userName);           //returns list of groups to user.
            case "SFT" -> sendFileToUser(splitStr);                             //Sends file to other user.
            case "RFFA" -> receiveFileFromOtherUserAccepted(splitStr);          //Receive file from user accepted.
            case "CSW" -> forwardPublicKey(splitStr);                           //Forwards publicKey.
            default -> serverUtil.sendMessageToClient("400 Unkown command", userName);
        }
    }

    private void addFileTransferManagerProces(String userName, String targetUserName, ServerUtil serverUtil, String[] splitStr) {
        if (fileTransferManagerThread == null) {
            startFileTransferManagerThread(userName, targetUserName, serverUtil, splitStr);
            System.out.println(fileTransferManagerThread);
        } else if (!fileTransferManagerThread.isAlive()) {
            startFileTransferManagerThread(userName, targetUserName, serverUtil, splitStr);
        } else {
            serverUtil.sendMessageToClient(userName, "There already is a fileTransfer procces active!");
        }
    }

    private void startFileTransferManagerThread(String userName, String targetUserName, ServerUtil serverUtil, String[] splitStr) {
        FileTransferManager fileTransferManager = new FileTransferManager(userName, targetUserName, serverUtil, splitStr, fileTransferSocket);
        serverUtil.putFileTransferManagerInMap(userName, fileTransferManager);
        fileTransferManagerThread = new Thread(fileTransferManager, "FileTransferManager procces " + userName + " " + UUID.randomUUID());
        fileTransferManagerThread.start();
    }

    private String extractFromString(String message, int begin, int end) {
        return message.substring(begin, end);
    }

    private void pong(String pong) {
        if (!(System.currentTimeMillis() <= (lastPong + pongWithinSec))) {
            System.out.println("Closed client " + userName + " because there wasn't a pong");
            String message = userName + " disconnected";
            closeClient(message, true);
        }
        if (pong.equals(CMD_PONG)) {
            lastPong = System.currentTimeMillis();
        }
    }

    //todo refactor
    private void closeClient(String message, boolean disconnected) {
        serverUtil.broadcastMessageToAllUsers(message);
        serverUtil.broadcastMessageToAllUsers("REMOVE_SHARED_KEY " + userName);
        if (disconnected) {
            serverUtil.sendMessageToClient("DSCN Pong timeout", userName);
        } else {
            serverUtil.sendMessageToClient("200 Goodbye", userName);

        }
        try {
            serverUtil.deleteClient(userName);
            serverUtil.removeUserFromAllGroups(userName);
            serverUtil.removeFileTransferSocketByKey(userName);
            fileTransferSocket.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void statusGroupKick(String[] splitStr) {
        if (splitStr.length >= 2) {
            serverUtil.statusVoteToKickProcces(splitStr[1]);
        }
    }

    private void startVoteToKickInGroup(String[] splitStr) {
        if (splitStr.length >= 3) {
            serverUtil.startVoteToKickInGroup(splitStr[1], splitStr[2], 1);
        }
    }

    private void kickGroupMember(String[] splitStr) {
        if (splitStr.length >= 3) {
            if (splitStr[2].equalsIgnoreCase("y") || splitStr[2].equalsIgnoreCase("yes")) {
                serverUtil.voteToKickInGroup(splitStr[1], userName, true);
            } else if (splitStr[2].equalsIgnoreCase("n") || splitStr[2].equalsIgnoreCase("no")) {
                serverUtil.voteToKickInGroup(splitStr[1], userName, false);
            }
        }
    }

    private void createGroup(String[] splitStr) {
        if (splitStr.length >= 2) {
            serverUtil.createGroup(userName, splitStr[1]);
        }
    }

    private void removeUserFromGroup(String[] splitStr) {
        if (splitStr.length >= 2) {
            serverUtil.removeUserFromGroup(splitStr[1], userName, "");
        }
    }

    private void addUserToGroup(String[] splitStr) {
        if (splitStr.length >= 2) {
            serverUtil.addUserToGroup(splitStr[1], userName, client);
        }
    }

    private void messageGroup(String[] splitStr, String line) {
        if (splitStr.length >= 3) {
            int removeFromLine = splitStr[0].length() + splitStr[1].length() + 2;
            line = extractFromString(line, removeFromLine, line.length());
            line = userName + ": " + line;
            serverUtil.broadcastMessageToGroupAsUser(splitStr[1], userName, line);
        }
    }

    private void returnAllUserList() {
        String message = serverUtil.getUsers().toString();
        serverUtil.sendMessageToClient(message, userName);
    }

    private void broadcastToAllUsers(String[] splitStr, String line) {
        line = extractFromString(line, splitStr[0].length() + 1, line.length());
        String message = "[Broadcast] " + userName + ": " + line;
        System.out.println("                                                                                       broadcast command");
        serverUtil.broadcastMessageToAllUsers(message);
    }

    private void pmUser(String[] splitStr, String line) {
        if (splitStr.length >= 2) {
            System.out.println(line);
            line = GeneralUtility.removeWord(line, splitStr[0] + " " + splitStr[1]);
            String message = "[PM] " + userName + " " + line;
            System.out.println("deze line heb ik verstuurd: " + line);
            System.out.println("                                                                                       PM command");
            serverUtil.sendMessageToClient(message, splitStr[1]);
        }
    }

    //todo refactor
    private void quitUser() {
        System.out.println("                                                                                       QUIT command");
        String message = userName + " logged off.";
        closeClient(message, false);
    }

    private void sendFileToUser(String[] splitStr) {
        System.out.println("SFT active " + splitStr.length);
        if (splitStr.length >= 4) {
            System.out.println(splitStr);
            System.out.println(splitStr[2]);
            addFileTransferManagerProces(userName, splitStr[2], serverUtil, splitStr);
        }
    }

    //todo testen
    private void receiveFileFromOtherUserAccepted(String[] splitStr) {
        System.out.println("RFFA accepted");
        if (splitStr.length >= 2) {
            serverUtil.getFileTransferManager(splitStr[1]).setReceiverAccepted(true);
        }
    }

    private void forwardPublicKey(String[] splitStr) {
        if (splitStr.length >= 4) {
            String targetUserName = splitStr[1];
            System.out.println("Sender public key to " + targetUserName + " from " + userName);
            serverUtil.sendMessageToClient("PUBLICKEY " + userName + " " + splitStr[2] + " " + splitStr[3], targetUserName);
        }
    }
}
