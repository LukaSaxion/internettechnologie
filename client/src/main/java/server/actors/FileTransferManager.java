package server.actors;

import server.utlity.GeneralUtility;
import server.utlity.ServerUtil;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;


/**
 * Sources used to code this:
 * https://howtodoinjava.com/java/multi-threading/callable-future-example/
 * https://bitek.dev/blog/java_threading_shared_data_tutorial/
 */

/**
 * Saves & stores & sends files from/to clients.
 */

public class FileTransferManager implements Runnable {
    private static final String RECEIVE_FILE = "RECEIVE_FILE";
    private final String targetUserName;
    private final String userName;
    private final String[] splitStr;
    private final int durationLengthForTargetResponse = 30; //In seconds.
    private final ServerUtil serverUtil;
    private final GeneralUtility generalUtility;
    private final Socket fileTransferSocket;
    private String checkSum = "";
    private boolean receiverAccepted = false;

    public FileTransferManager(String userName, String targetUserName, ServerUtil serverUtil, String[] splitStr, Socket fileTransferSocket) {
        this.userName = userName;
        this.targetUserName = targetUserName;
        this.serverUtil = serverUtil;
        this.splitStr = splitStr;
        this.generalUtility = new GeneralUtility();
        this.fileTransferSocket = fileTransferSocket;
    }

    @Override
    public void run() {
        String fileName = "";
        String path = "";
        boolean encrypted = false;
        if (splitStr.length >= 4) {
            fileName = splitStr[1];
            path = "./files/server_files/" + fileName;
            int fileSize = generalUtility.tryParse(splitStr[3]);
            if (generalUtility.tryParse(splitStr[4]) == 1) encrypted = true;
            System.out.println("FileName: " + fileName);
            System.out.println("FileSize: " + fileSize + " bytes");
            System.out.println("FileEncrypted: " + encrypted);
            saveFileFromClient(fileName, path, fileSize);

            String message = "";
            //Send request to target user.
            message = userName + " requested an FileTransfer for " + fileName + " respond within " + durationLengthForTargetResponse + " seconds to accept.\nUse [RFFA " + userName + "] to accept.";
            serverUtil.sendMessageToClient(message, targetUserName);

            try {
                TimeUnit.SECONDS.sleep(durationLengthForTargetResponse/2);
                if (!receiverAccepted) {
                    message = "Respond within " + (durationLengthForTargetResponse / 2) + " seconds to accept file: " + fileName + "  from: " + userName + "\nUse [RFFA " + userName + "] to accept.";
                    serverUtil.sendMessageToClient(message, targetUserName);
                    TimeUnit.SECONDS.sleep(durationLengthForTargetResponse / 2);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (receiverAccepted) {
                sendFileToClient(path, splitStr[2], userName, splitStr[4]);
            } else {
                message = targetUserName + " did not accept FileTransfer for file: " + fileName;
                serverUtil.sendMessageToClient(message, userName);
            }
        }
        //remove FileTransferManager from map.
        serverUtil.deleteFileTransferManagerFromMap(userName);
    }

    //Source: https://gist.github.com/CarlEkerot/2693246
    private void sendFileToClient(String filePath, String targetUserNameKey, String fromUserNameKey, String encryped) {
        //Getting target fileTransferSocket
        Socket targetFileTransferSocket = serverUtil.getFileTransferSocketByKey(targetUserNameKey);
        System.out.println("SFTC");
        //Creating file to print location later.
        File file = new File(filePath);
        serverUtil.sendMessageToClient(RECEIVE_FILE + " " + file.getName() + " " + fromUserNameKey + " " + file.length() + " " + encryped, targetUserNameKey);
        System.out.println("File length: " + file.length());

        System.out.println(file.getAbsolutePath());
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(targetFileTransferSocket.getOutputStream());
            //Write received checksum to client DOS.
            dataOutputStream.writeUTF(checkSum);
            FileInputStream fileInputStream = new FileInputStream(filePath);

            byte[] buffer = new byte[(int) file.length()];

            //Print data to console
            System.out.println(new String(Files.readAllBytes(Paths.get(filePath))));

            //Write data to stream
            while (fileInputStream.read(buffer) > 0) {
                dataOutputStream.write(buffer);
            }
            dataOutputStream.flush();
            serverUtil.sendMessageToClient("[Server] has send the file: " + file.getName() + " " + file.length() + " bytes" + " to user: " + targetUserNameKey, fromUserNameKey);
            System.out.println("Server has send the file: " + file.getName() + " " + file.length() + " bytes" + " to user: " + targetUserNameKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Source: https://gist.github.com/CarlEkerot/2693246
    private void saveFileFromClient(String fileName, String path, int fileSize) {
        //Creating file to print location later.
        File file = new File(path);
        //Create parent directories if not exists
        file.getParentFile().mkdirs();
        try {
            DataInputStream dataInputStream = new DataInputStream(fileTransferSocket.getInputStream());
            checkSum = dataInputStream.readUTF();
            FileOutputStream fileOutputStream = new FileOutputStream(path);

            byte[] buffer = new byte[fileSize];

            int read = 0;
            int totalRead = 0;
            int remaining = fileSize;

            while ((read = dataInputStream.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
                totalRead += read;
                remaining -= read;
                System.out.println("Total read: " + totalRead + " bytes" + " remaining bytes: " + remaining);
                //write bytes to file.
                fileOutputStream.write(buffer, 0, read);
                //print write to console
                System.out.println("Content of file: ");
                System.out.write(buffer, /* start */ 0, /* length */ read);
            }
            fileOutputStream.close();
            System.out.println("\nFile: " + fileName + " | received from: " + userName + " | Stored at: " + file.getAbsolutePath());
            serverUtil.sendMessageToClient("[Server] " + fileName + " received!", userName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setReceiverAccepted(boolean receiverAccepted) {
        this.receiverAccepted = receiverAccepted;
    }
}
