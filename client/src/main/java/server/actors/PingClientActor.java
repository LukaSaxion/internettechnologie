package server.actors;


import server.utlity.ServerUtil;

public class PingClientActor implements Runnable {
    private ServerUtil serverUtil;
    private int ping_delay; //3sec

    public PingClientActor(ServerUtil serverUtil, int ping_delay) {
        this.serverUtil = serverUtil;
        this.ping_delay = ping_delay;
    }

    @Override
    public void run() {
        while (true){
            serverUtil.broadcastMessageToAllUsers("PING");
            serverUtil.wait(ping_delay);
        }
    }
}
