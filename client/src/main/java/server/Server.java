package server;

import server.actors.FileTransferManager;
import server.actors.PingClientActor;
import server.actors.ServerMessageActor;
import server.model.Group;
import server.utlity.ServerUtil;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private final boolean SHOULD_PING = true;
    private static final String SERVER_VERSION = "0.1";
    private static final int SERVER_PORT = 3001;
    private static final int FILE_SERVER_PORT = 3002;
    private static final int PING_DELAY_SEC = 3;
    private Map<String, Socket> clients; //Key = username
    private Map<String, Socket> clientsFileTransfer;  //Key = username
    private Map<String, Group> groupMap;  //Key = group name
    private Map<String, FileTransferManager> fileTransferManagerMap; //Key = Sender username


    //Sources used: https://www.chegg.com/homework-help/questions-and-answers/using-java-assignment-implement-basic-key-transport-protocol-key-aes-sent-client-server-us-q47638952

    public static void main(String[] args) {
        new Server().run();
    }

    public void run() {
        System.out.println("Server started on port: " + SERVER_PORT + " | Server version: " + SERVER_VERSION);
        clients = Collections.synchronizedMap(new HashMap<>());
        clientsFileTransfer = Collections.synchronizedMap(new HashMap<>());
        groupMap = Collections.synchronizedMap(new HashMap<>());
        fileTransferManagerMap = Collections.synchronizedMap(new HashMap<>());
        ExecutorService threadPool = Executors.newCachedThreadPool();

        ServerSocket serverSocket = null;
        ServerSocket fileServerSocket = null;
        try {
            serverSocket = new ServerSocket(SERVER_PORT);
            fileServerSocket = new ServerSocket(FILE_SERVER_PORT);
        } catch (IOException e) { //if any IO errors with constructing the socket
            System.err.println("Could not listen on port: " + SERVER_PORT + " or " + FILE_SERVER_PORT + ".");
            System.exit(1); //quit the program if there is an error }
        }

        while (true) {
            Socket socket = null;
            Socket fileTransferSocket = null;
            String userName = "";
            try {
                socket = serverSocket.accept();
                fileTransferSocket = fileServerSocket.accept();
                OutputStream outputStream = socket.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream);
                InputStream inputStream = socket.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

//              Welcome new client
                printWriter.println("INFO Welcome to the server " + SERVER_VERSION);
                printWriter.flush();

                userName = userLogin(bufferedReader, printWriter, socket);
                if (!userName.equals("")) {
                    clients.put(userName, socket);
                    clientsFileTransfer.put(userName, fileTransferSocket);
                    ServerUtil serverUtil = new ServerUtil(this);
                    //todo testen
                    if (SHOULD_PING) {
                        PingClientActor pingClientActor = new PingClientActor(serverUtil, PING_DELAY_SEC);
                        threadPool.submit((pingClientActor));
                    }
                    ServerMessageActor clientMessageActor = new ServerMessageActor(bufferedReader, socket, userName, (PING_DELAY_SEC * 3), serverUtil, SHOULD_PING, fileTransferSocket);
                    threadPool.submit((clientMessageActor));
                }
            } catch (IOException e) { //if any errors
                System.err.println("Accept failed.");
            }
        } // end while loop
    }

    public String userLogin(BufferedReader bufferedReader, PrintWriter printWriter, Socket socket) {
        String line = "";
        while (true) {
            try {
                System.out.println("User trying to login from: " + socket);
                line = bufferedReader.readLine();
                System.out.println(line);
                String[] splitStr = line.split("\\s+");
                if (splitStr.length >= 2 && splitStr[0].equals("CONN")) {
                    if (validUsernameFormat(splitStr[1])) {
                        //Valid username
                        if (!userExists(splitStr[1])) {
                            //Unique username
                            sendMessageToClient("200 " + splitStr[1], printWriter);
                            return splitStr[1];
                        } else {
                            //Username already in use
                            sendMessageToClient("401 User already logged in", printWriter);
                        }
                    } else {
                        //Not a valid username
                        sendMessageToClient("400 Username has an invalid format (only characters, numbers and underscores are allowed)", printWriter);
                    }
                } else {
                    sendMessageToClient("Use 'CONN username' to login", printWriter);
                }
            } catch (IOException e) {
                System.err.println("Something went wrong with login from: " + socket);
                break;
            }
        }
        return "";
    }

    private boolean userExists(String username) {
        return clients.containsKey(username);
    }

    private boolean validUsernameFormat(String username) {
        String pattern = "[a-zA-Z0-9_]{3,14}";
        return username.matches(pattern);
    }

    private void sendMessageToClient(String message, PrintWriter printWriter) {
        System.out.println(message);
        printWriter.println(message);
        printWriter.flush();
    }

    public Map<String, Socket> getClients() {
        return clients;
    }

    public Map<String, Socket> getClientsFileTransfer() {
        return clientsFileTransfer;
    }

    public void setClients(Map<String, Socket> clients) {
        this.clients = clients;
    }

    public Group getGroup(String groupName) {
        return groupMap.get(groupName);
    }

    public void putGroup(String groupName, Group group) {
        groupMap.put(groupName, group);
    }

    public Map<String, Group> getGroupMap() {
        return groupMap;
    }

    public void setGroupMap(Map<String, Group> groupMap) {
        this.groupMap = groupMap;
    }

    public FileTransferManager getFileTransferManager(String senderName) {
        return fileTransferManagerMap.get(senderName);
    }

    public void deleteFileTransferManagerFromMap(String senderName) {
        fileTransferManagerMap.remove(senderName);
    }

    public void putFileTransferManagerInMap(String senderName, FileTransferManager fileTransferManager) {
        fileTransferManagerMap.put(senderName, fileTransferManager);
    }
}
