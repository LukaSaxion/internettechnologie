package server.utlity;

import server.Server;
import server.actors.FileTransferManager;
import server.model.Group;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ServerUtil {
    private Server server;
    private Map<String, Socket> clients;
    private Socket client = null;
    private Group group;
    private Map<String, Group> groupMap;
    private OutputStream outputStream;
    private PrintWriter printWriter;

    public ServerUtil(Server server) {
        this.server = server;
    }

    public void wait(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Socket getFileTransferSocketByKey(String key) {
        return server.getClientsFileTransfer().get(key);
    }

    public void removeFileTransferSocketByKey(String key) {
        server.getClientsFileTransfer().remove(key);
    }

    //todo testen
    public void broadcastMessageToAllUsers(String message) {
        clients = server.getClients();
        broadcastMessage(clients, message);
    }

    //todo testen
    public void broadcastMessageToGroup(String groupName, String message) {
        group = server.getGroup(groupName);
        String endMessage = "[" + groupName + "] ";
        endMessage = endMessage + message;
        broadcastMessage(group.getClients(), endMessage);
    }

    public void broadcastMessageToGroupAsUser(String groupName, String userName, String message) {
        if (userIsInGroup(groupName, userName)) {
            broadcastMessageToGroup(groupName, message);
        }
    }

    public boolean userIsInGroup(String groupName, String userName) {
        group = server.getGroup(groupName);
        clients = group.getClients();
        return clients.containsKey(userName);
    }

    public void listAllGroupsToUser(String userName) {
        sendMessageToClient(getAllGroupNames().toString(), userName);
    }

    private ArrayList<String> getAllGroupNames() {
        ArrayList<String> groupNames = new ArrayList<>();
        groupMap = server.getGroupMap();
        groupMap.forEach((k, group) -> {
            groupNames.add(k);
        });
        return groupNames;
    }

    private void broadcastMessage(Map<String, Socket> clients, String message) {
        clients.forEach((k, v) -> {
            sendMessageToClient(message, k);
        });
    }

    public void addUserToGroup(String groupName, String userName, Socket userClient) {
        group = server.getGroup(groupName);
        clients = group.getClients();
        clients.put(userName, userClient);
        group.setClients(clients);
        server.putGroup(groupName, group);
        broadcastMessageToGroup(groupName, "User " + userName + " joined the group!");
    }

    //todo testen
    public void removeUserFromGroup(String groupName, String userName, String message) {
        group = server.getGroup(groupName);
        clients = group.getClients();
        clients.remove(userName);
        group.setClients(clients);
        server.putGroup(groupName, group);
        //todo test message?
        if (message == null || message.equals("")) {
            broadcastMessageToGroup(groupName, "User " + userName + " leaved the group!");
        } else {
            broadcastMessageToGroup(groupName, message);
        }
    }

    public void removeUserFromAllGroups(String userName) {
        groupMap = server.getGroupMap();
        groupMap.forEach((k, group) -> {
            clients = group.getClients();
            clients.forEach(((s, socket) -> {
                if (s.equals(userName)) {
                    clients.remove(s);
                }
            }));
        });
        server.setGroupMap(groupMap);
    }

    public void startVoteToKickInGroup(String groupName, String kickThisUserName, long voteDuractionInMin) {
        group = server.getGroup(groupName);
        System.out.println(group.toString());
        group.addKickUserProcces(kickThisUserName, voteDuractionInMin, this);
    }

    public void statusVoteToKickProcces(String groupName) {
        group = server.getGroup(groupName);
        System.out.println(group.toString());
        group.statusVoteToKick(this);
    }

    public void voteToKickInGroup(String groupName, String voterName, boolean vote) {
        group = server.getGroup(groupName);
        System.out.println(group.toString());
        group.voteOnKick(voterName, vote, this);
    }

    public void createGroup(String userName, String groupName) {
        groupMap = server.getGroupMap();
        clients = server.getClients();
        client = clients.get(userName);
        group = new Group(client, userName, groupName);
        groupMap.put(groupName, group);
        server.setGroupMap(groupMap);
        broadcastMessageToGroup(groupName, "Group: " + groupName + " created!");
    }

    public void sendMessageToClient(String message, String userNameKey) {
        clients = server.getClients();
        client = clients.get(userNameKey);
        //Schrijven
        try {
            outputStream = client.getOutputStream();
            printWriter = new PrintWriter(outputStream);
            printWriter.println(message);
//            System.out.println("Message send to " + userNameKey + " Message: " + message);
            printWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteClient(String client) {
        //Clients
        clients = server.getClients();
        clients.remove(client);
        System.out.println(clients);
        server.setClients(clients);
    }

    public ArrayList<String> getUsers() {
        clients = server.getClients();
        ArrayList<String> userList = new ArrayList<>();
        clients.forEach(((k, socket) -> {
            userList.add(k);
        }));
        return userList;
    }

    public Socket getClientByUsername(String userName) {
        return server.getClients().get(userName);
    }

    public void setClient(Socket client) {
        this.client = client;
    }

    public FileTransferManager getFileTransferManager(String senderName) {
        return server.getFileTransferManager(senderName);
    }

    public void deleteFileTransferManagerFromMap(String senderName) {
        server.deleteFileTransferManagerFromMap(senderName);
    }

    public void putFileTransferManagerInMap(String senderName, FileTransferManager fileTransferManager) {
        server.putFileTransferManagerInMap(senderName, fileTransferManager);
    }
}
